<?php

namespace Drupal\Tests\relatedbyterms\Kernel;

use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Test related nodes.
 *
 * @group relatedbyterms
 */
class RelatedNodesTest extends KernelTestBase {

  /** @var \Drupal\relatedbyterms\RelatedByTermsServiceInterface $service */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'language',
    'node',
    'user',
    'taxonomy',
    'field',
    'text',
    'locale',
    'relatedbyterms',
    'relatedbyterms_test',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('user');
    $this->installSchema('system', ['key_value_expire']);

    $this->installConfig([
      'node',
      'field',
      'taxonomy',
      'locale',
      'relatedbyterms',
      'relatedbyterms_test',
      'filter',
    ]);

    $this->createContent();
    $this->service = $this->container->get('relatedbyterms.manager');
  }

  /**
   * Create contents for testing.
   */
  protected function createContent() {

    $terms = [
      [
        'vid' => 'tags',
        'name' => 'Tag 1',
      ],
      [
        'vid' => 'tags',
        'name' => 'Tag 2',
      ],
      [
        'vid' => 'tags',
        'name' => 'Tag 3',
      ],
    ];

    foreach ($terms as $key => $term) {
      $terms[$key] = $this->createTerm($term);
    }

    $nodes = [
      [
        'nid' => '1',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'en',
        'field_test_tags' => $terms[0]->id(),
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '2',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'en',
        'field_test_tags' => $terms[0]->id(),
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '3',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'en',
        'field_test_tags' => [$terms[0]->id(),$terms[1]->id(),$terms[2]->id()],
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '4',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'en',
        'field_test_tags' => [$terms[1]->id(),$terms[2]->id()],
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '5',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'en',
        'field_test_tags' => [$terms[2]->id()],
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '6',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'es',
        'field_test_tags' => [$terms[2]->id()],
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],
      [
        'nid' => '7',
        'title' => $this->randomMachineName(8),
        'type' => 'rbyt_test',
        'langcode' => 'es',
        'field_test_tags' => [$terms[2]->id()],
        'promote' => FALSE,
        'sticky' => FALSE,
        'body' => $this->randomMachineName(20),
      ],

    ];

    foreach ($nodes as $key => $node) {
      $nodes[$key] = $this->createNode($node);
    }

  }

  /**
   * Creates a taxonomy term from array data.
   */
  protected function createTerm(array $data) {
    $term = Term::create($data);
    $term->save();
    return $term;
  }

  /**
   * Creates a node from array data.
   */
  protected function createNode(array $data) {
    $node = Node::create($data);
    $node->save();
    return $node;
  }

  /**
   * Test simple related nodes.
   */
  public function testRelatedNodes() {
    $nids = $this->service->getRelatedNodes('1', 'en');
    $this->assertEqual(count($nids), 2, 'Related nodes matches.');
  }

  /**
   * Test related nodes order by number of matching tags.
   */
  public function testRelatedOrder() {
    $nids = $this->service->getRelatedNodes('3', 'en');
    $this->assertEqual(count($nids), 4, 'Related nodes matches.');
    $this->assertEqual(
      [4,5,2,1],
      $nids,
      "Related nodes order matches."
    );
  }

  /**
   * Test related nodes in a different language.
   */
  public function testRelatedLanguage() {
    $nids = $this->service->getRelatedNodes('6', 'es');
    $this->assertEqual(count($nids), 1, 'Related nodes matches.');
  }

}
